<?php
$head = "
<html>
<head>
<title>Site Title</title>
<script src=\"jquery-min.js\" type=\"text/javascript\"></script>
</head>
<body>
<div id=\"images\" onclick=\"imgchg.stst();\">Loading...</div>
";

  $app_id = 'Facebook App Id';  // From Facebook App Page
  $album_id = 'Facebook Album Id'; // Used API example to look up
  $app_secret = 'Facebook App Secret'; // From Facebook App Page
  $my_url = 'http://www.domain.tld/slideshow.php';

  $code = $_REQUEST["code"];
 
 if(empty($code)) {
    $dialog_url = 'https://www.facebook.com/dialog/oauth?client_id=' 
    . $app_id . '&redirect_uri=' . urlencode($my_url) ;
    echo("<script>top.location.href='" . $dialog_url . "'</script>");
  }

  $token_url = 'https://graph.facebook.com/oauth/access_token?client_id='
    . $app_id . '&redirect_uri=' . urlencode($my_url) 
    . '&client_secret=' . $app_secret 
    . '&code=' . $code;
  $access_token = file_get_contents($token_url);
 
  $fql_query_url = 'https://graph.facebook.com/'
    . '/fql?q=SELECT+pid,src,src_big,caption+FROM+photo+WHERE+aid="'.$album_id.'"'
    . '&' . $access_token;
  $fql_query_result = file_get_contents($fql_query_url);
  $fql_query_obj = json_decode($fql_query_result, true);

  $content = "
  <script type=\"text/javascript\"> 
  var imgchg = {
  p: 0,
  img: Array(),
  st: 0,
  stst: function()
  {
    this.st = this.st?0:1;
    this.changeimg();
  },
  changeimg : function()
  {
    if(this.st)
    {
      if(this.p >= this.img.length)
        this.p=0;
      $(images).empty();
      $(images).append(this.img[this.p]);
      this.p++;
      setTimeout(\"imgchg.changeimg()\",3000);
    }
  },
  init: function()
  {
    this.st = 1;
    this.p=0;
  ";
  $pid=0;
  foreach($fql_query_obj['data'] as $photo)
  {
    $content .= "    this.img[".$pid."] = new Image();\n";
    $content .= "    this.img[".$pid."].src = \"".$photo['src_big']."\";\n";
    $pid++;
  }
$content .= "
     this.changeimg();
   }};
  imgchg.init();
</script>
";
$foot = "
</body>
</html>
";
file_put_contents("slideshow.html", $head . $content . $foot);
echo $head . $content . $foot;
?>
